import React, { Component } from "react";
import Axios from "axios";

let view;
let id;
export default class Views extends Component {
  constructor() {
    super();
    this.state = {
      id: "",
      data: {},
    };
  }
  componentDidMount() {
    Axios.get(`http://110.74.194.124:15011/v1/api/articles/${id}`)
      .then((res) => {
        this.setState({
          data: res.data.DATA,
        });
      })
      .catch((err) => {
        console.log(err);
      });
  }
  render(props) {
    var artID = this.props.match.params.id;
    id = artID;

    return (
      <div className="container text-left mt-3">
        <h1>Article</h1> {view}
        <div className="row">
          <div className="col-4">
          <img
           src={
            this.state.data.IMAGE === null
              ? "https://www.renovabike.it/wp-content/themes/gecko/assets/images/placeholder.png"
              : this.state.data.IMAGE
          }
          alt=""
          width="100%"
          />
          </div>
          <div className="col-8">
            <h1>{this.state.data.TITLE}</h1>
            <p>{this.state.data.DESCRIPTION}</p>
          </div>
        </div>
      </div>
    );
  }
}
