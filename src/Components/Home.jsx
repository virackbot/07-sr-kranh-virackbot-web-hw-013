import React, { Component } from "react";
import { Table } from "react-bootstrap";
import { Link } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import Axios from "axios";
import List from "./List";

// const artbtn = {
//   padeling: "20px",
// };

export default class Home extends Component {
  constructor() {
    super();
    this.substring = this.substring.bind(this);
    this.delete = this.delete.bind(this);
    this.state = {
      data: [],
      updating: 1,
    };
  }

  delete = (id, index) => {
    Axios.delete(
      `http://110.74.194.124:15011/v1/api/articles/${id}
          `
    )
      .then((res) => {
        console.log(res);
      })
      .catch((err) => {
        console.log(err);
      });
    alert(" YOU DELETED SUCCESSFULLY");
    var del = [...this.state.data];
    if (index !== -1) {
      del.splice(index, 1);
      this.setState(() => {
        return (this.state.data = del);
      });
    }
  };

  componentWillMount() {
    Axios.get("http://110.74.194.124:15011/v1/api/articles?page=1&limit=15")
      .then((res) => {
        this.setState({
          data: res.data.DATA,
        });
      })
      .catch((err) => {
        console.log(err);
      });
  }
  substring = (art) => {
    let year = art.substring(0, 4);
    let month = art.substring(4, 6);
    let day = art.substring(6, 8);
    let Date = [year, month, day];
    return Date.join("-");
  };

  render() {
    const list = this.state.data.map((d, index) => (
      <List
        data={d}
        getDelete={this.getDelete}
        delete={this.delete}
        index={index}
        key={d.ID}
      />
    ));

    return (
      <div className="container ">
        <div className="mb-3 mt-4" >
          <h1>Article management</h1>
          <Link as={Link} to="/add">
            <button className="btn btn-primary">Add new Article</button>
          </Link>
        </div>

        <Table className="container table-hover table-bordered" >
          <thead>
            <tr>
              <th>#</th>
              <th>Title</th>
              <th  style={{width:"20%"}} >Desription</th>
              <th  style={{width:"12%"}}>Created Date</th>
              <th style={{width:"14%"}}>Image</th>
              <th style={{width: "19%"}}>Action</th>
            </tr>
          </thead>
          <tbody>{list}</tbody>
        </Table>
      </div>
    );
  }
}
