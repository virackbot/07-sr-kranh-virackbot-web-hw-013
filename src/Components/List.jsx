import React, { Component } from "react";
import { Image } from "react-bootstrap";
import { Link } from "react-router-dom";

export default class List extends Component {
  constructor() {
    super();
    this.substring = this.substring.bind(this);
    this.getDelete = this.getDelete.bind(this);
  }
  getDelete = () => {
    this.props.delete(this.props.data.ID, this.props.index);
  };

  substring = (art) => {
    let year = art.substring(0, 4);
    let month = art.substring(4, 6);
    let day = art.substring(6, 8);
    let Date = [ day,month,year];
    return Date.join("-");
  };

  render(props) {
    return (
      <tr key={this.props.data.ID}>
        <td>{this.props.data.ID}</td>
        <td>{this.props.data.TITLE}</td>
        <td className="text-left">{this.props.data.DESCRIPTION}</td>
        <td>{this.substring(this.props.data.CREATED_DATE)}</td>
        <td>
          <Image
          className="align-middle"
            id="image"
            src={
              this.props.data.IMAGE === null
                ? "https://www.renovabike.it/wp-content/themes/gecko/assets/images/placeholder.png"
                : this.props.data.IMAGE
            }
            width="170px"
          ></Image>
        </td>
        <td>
          <Link as={Link} to={`/view/${this.props.data.ID}`}>
            {" "}
            <button className="btn btn-sm btn-primary">View</button>
          </Link>
          <Link as={Link} to={`/add?isUpdating=true&&id=${this.props.data.ID}`}>
            <button className="btn btn-sm btn-warning mx-1 my-1">Edit</button>
          </Link>
          <button className="btn btn-sm btn-danger" onClick={this.getDelete}>
            Delete
          </button>
        </td>
      </tr>
    );
  }
}
