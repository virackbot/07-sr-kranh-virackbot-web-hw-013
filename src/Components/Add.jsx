import React, { Component } from "react";
import {  Form, Button } from "react-bootstrap";
import Axios from "axios";
import queryString from "query-string";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Redirect,
} from "react-router-dom";



export default class Add extends Component {
  constructor(props) {
    super();
    var a = queryString.parse(props.location.search);
    this.state = {
      isUpdating: a.isUpdating,
      url: "",
      updateid: a.id,
    };
  }
  componentWillMount() {
    if (this.state.isUpdating == "true") {
      Axios.get(
        `http://110.74.194.124:15011/v1/api/articles/${this.state.updateid}`
      )
        .then((res) => {
          this.url.value = res.data.DATA.IMAGE;
          this.title.value = res.data.DATA.TITLE;
          this.desc.value = res.data.DATA.DESCRIPTION;
          this.setState({
            url: res.data.DATA.IMAGE,
          });
        })
        .catch((err) => {
          console.log(err);
        });
    }
  }
  updatearticle = (d) => {
    Axios.put(
      `http://110.74.194.124:15011/v1/api/articles/${this.state.updateid}`,
      {
        TITLE: d.TITLE,
        DESCRIPTION: d.DESCRIPTION,
        IMAGE: d.IMAGE,
      }
    ).then((res) => {
      console.log(res.data);
      alert(res.data.MESSAGE);
      this.props.history.push("/");
    });
  };

  addarticle = (d) => {
    Axios.post("http://110.74.194.124:15011/v1/api/articles", {
      TITLE: d.TITLE,
      DESCRIPTION: d.DESCRIPTION,
      IMAGE: d.IMAGE,
    }).then((res) => {
      console.log(res.data);
      alert(res.data.MESSAGE);
      this.props.history.push("/");
    });
  };
  
  onSubmit = () => {
    var url = this.url.value;
    var title = this.title.value;
    var desc = this.desc.value;
    if (title === "" || desc === "" || url === "" ) {
      this.validDesc.textContent = "Please input description !";
      this.validTitle.textContent = "Please input title ! ";
      this.valideUrl.textContent = " Please add image url !";
    } else {
      this.validDesc.textContent = "";
      this.validTitle.textContent = "";
      this.valideUrl.textContent = "";

      let article = {
        TITLE: title,
        DESCRIPTION: desc,
        IMAGE: url,
      };
      if (this.state.isUpdating === undefined) {
        this.addarticle(article);
        this.setState({
          url: url,
        });
      } else {
        this.updatearticle(article);
        this.setState({
          url: url,
        });
      }
    }
  };
  render() {
    return (
      <div className="container mt-3">
        <div className="row text-left">
          <div className="col-8" >
            <h1>
              {this.state.isUpdating === undefined
                ? "Add Article"
                : "Update Article"}
            </h1>
            <Form.Group>
              <Form.Label >TITLE :</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter Title"
                ref={(title) => (this.title = title)}
              />
              <span
                ref={(validTitle) => (this.validTitle = validTitle)}
                style={{ color: "red" }}
              ></span>
              <br />
              <Form.Label>DESCRIPTION :</Form.Label>
              <Form.Control
               as="textarea" 
               rows="5"
                type="text"
                placeholder="Enter Description"
                ref={(desc) => (this.desc = desc)}
              />
              <span
                ref={(validDesc) => (this.validDesc = validDesc)}
                style={{ color: "red" }}
              ></span>
              <br />
              <Form.Label >IMAGE URL :</Form.Label>

              <Form.Control
                type="text"
                placeholder="Enger image url"
                ref={(url) => (this.url = url)}
              />
              <span
                ref={(valideUrl) => (this.valideUrl = valideUrl)}
                style={{ color: "red" }}
              ></span>
              <br />
            </Form.Group>
            <Button
              type="submit"
              className={
                this.state.isUpdating === undefined
                  ? "btn btn-primary"
                  : "btn btn-warning"
              }
              onClick={this.onSubmit}
            >
              {this.state.isUpdating === undefined ? "Submit" : "Update"}
            </Button>
          </div>
          <div className="col-4" style={{marginTop:"98px"}}>
            <img
              src={this.state.url === "" ? ph : this.state.url}
              alt="img"
              width="100%"
            />
          </div>
        </div>
      </div>
    );
  }
}
const ph =
  "https://www.renovabike.it/wp-content/themes/gecko/assets/images/placeholder.png";
