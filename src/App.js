import React from "react";
import logo from "./logo.svg";
import "./App.css";
import Home from "./Components/Home";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import Header from "./Components/Header";
import Add from "./Components/Add";
import Views from "./Components/Views";

function App() {
  return (
    <div className="App">
      <Router>
        <Header />
        <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/add" exact component={Add} />
          <Route path="/view/:id" exact component={Views} />
        </Switch>
      </Router>
    </div>
  );
}

export default App;
